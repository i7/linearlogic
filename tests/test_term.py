import unittest
import formula


class TermTest(unittest.TestCase):
    def setUp(self):
        self.x_sort = formula.ConstantSort([formula.Value(1),
                                            formula.Value(2),
                                            formula.Value(3)])
        self.y_sort = formula.ConstantSort([formula.Value(2),
                                            formula.Value(3),
                                            formula.Value(4),
                                            formula.Value(5)])
        self.x: formula.Variable = formula.Variable(
                "x", self.x_sort)
        self.y: formula.Variable = formula.Variable(
                "y", self.y_sort)
        self.v_val: formula.VariableValuation = {
                self.x: formula.Value(1),
                self.y: formula.Value(5)
                }

        self.addition = formula.ComputableFunction(
                "addition",
                2,
                lambda c_inter, t: formula.Value(t[0].value + t[1].value))

        self.addition_call = formula.FunctionCall(
                self.addition, (self.x, self.y))

    def test_basic_evaluation(self):
        c: formula.Value = formula.Value(9)

        self.assertEqual(self.x.evaluate((self.v_val, {}, {})),
                         self.v_val[self.x])
        self.assertEqual(self.y.evaluate((self.v_val, {}, {})),
                         self.v_val[self.y])
        self.assertEqual(c.evaluate(({}, {}, {})), c)
        self.assertEqual(c.evaluate((self.v_val, {}, {})), c)

    def test_variable_sorts(self):
        N: formula.Constant = formula.Constant("N")
        evals = [{N: formula.Value(1)}, {N: formula.Value(2)},
                 {N: formula.Value(3)}]
        one_value_sort: formula.VariableSort = formula.VariableSort(N, N)
        value_variable: formula.Variable = formula.Variable("var",
                                                            one_value_sort)
        for e in evals:
            self.assertEqual(one_value_sort.evaluate(e), [e[N]])
            var_valuations = value_variable.all_variable_valuations(e)
            self.assertEqual(len(var_valuations), 1)
            self.assertEqual(var_valuations, [{value_variable: e[N]}])

    def test_failing_evaluation(self):
        valuation: formula.VariableValuation = {
                self.x: formula.Value(0),  # out of bounds for x
                                           # and y missing
                }
        with self.assertRaises(ValueError):
            self.x.evaluate((valuation, {}, {}))
        with self.assertRaises(ValueError):
            self.y.evaluate((valuation, {}, {}))

    def test_basic_function(self):
        self.assertEqual(
                self.addition_call.evaluate((self.v_val, {}, {})),
                formula.Value(6))

    def test_variable_extraction(self):
        self.assertEqual(self.addition_call.variables, {self.x, self.y})
        self.assertEqual(self.x.variables, {self.x})
        self.assertEqual(self.y.variables, {self.y})

    def test_variable_valuation_generation(self):
        self.assertEqual(len(self.addition_call.all_variable_valuations(
            {})), len(self.x.values.values) * len(self.y.values.values))
        self.assertEqual([self.x.evaluate((e, {}, {}))
                          for e in self.x.all_variable_valuations({})],
                         self.x.values.values)

    def test_free_function_valuation_generation(self):
        f_sort = formula.ConstantSort([formula.Value(1),
                                       formula.Value(2),
                                       formula.Value(3)])
        f = formula.FreeFunction("f", 1, f_sort)
        g_sort = formula.ConstantSort([formula.Value(5),
                                       formula.Value(6)])
        # f(g(x, g(x, y))) translates to
        # f(g(1, g(1, 5))):
        # should give:
        #   + f(5) = 1g, g(1, 5) = 5
        #   + f(5) = 1g, g(1, 6) = 5, g(1, 5) = 6
        #   + f(5) = 2, g(1, 5) = 5
        #   + f(5) = 2, g(1, 6) = 5, g(1, 5) = 6
        #   + f(5) = 3, g(1, 5) = 5
        #   + f(5) = 3, g(1, 6) = 5, g(1, 5) = 6
        #   + f(6) = 1g, g(1, 6) = 6, g(1, 5) = 6
        #   + f(6) = 2, g(1, 6) = 6, g(1, 5) = 6
        #   + f(6) = 3, g(1, 6) = 6, g(1, 5) = 6
        g = formula.FreeFunction("g", 2, g_sort)
        g_inner = formula.FunctionCall(g, (self.x, self.y))
        g_outer = formula.FunctionCall(g, (self.x, g_inner))
        f_call = formula.FunctionCall(f, (g_outer,))
        expected_results = [
                {
                    (f, (formula.Value(5),)): formula.Value(1),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(5),
                },
                {
                    (f, (formula.Value(5),)): formula.Value(1),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(6),
                    (g, (formula.Value(1), formula.Value(6))):
                        formula.Value(5),
                },
                {
                    (f, (formula.Value(5),)): formula.Value(2),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(5),
                },
                {
                    (f, (formula.Value(5),)): formula.Value(2),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(6),
                    (g, (formula.Value(1), formula.Value(6))):
                        formula.Value(5),
                },
                {
                    (f, (formula.Value(5),)): formula.Value(3),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(5),
                },
                {
                    (f, (formula.Value(5),)): formula.Value(3),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(6),
                    (g, (formula.Value(1), formula.Value(6))):
                        formula.Value(5),
                },
                {
                    (f, (formula.Value(6),)): formula.Value(1),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(6),
                    (g, (formula.Value(1), formula.Value(6))):
                        formula.Value(6),
                },
                {
                    (f, (formula.Value(6),)): formula.Value(2),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(6),
                    (g, (formula.Value(1), formula.Value(6))):
                        formula.Value(6),
                },
                {
                    (f, (formula.Value(6),)): formula.Value(3),
                    (g, (formula.Value(1), formula.Value(5))):
                        formula.Value(6),
                    (g, (formula.Value(1), formula.Value(6))):
                        formula.Value(6),
                },
            ]
        all_valuations = f_call.all_function_valuations(self.v_val, {})
        self.assertEqual(len(expected_results), len(all_valuations))
        for v in all_valuations:
            if v not in expected_results:
                self.fail(f"{v} not expected")


if __name__ == '__main__':
    unittest.main()

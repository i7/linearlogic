import unittest
import formula


class FormulaTest(unittest.TestCase):
    def setUp(self):
        self.x_sort = formula.ConstantSort([formula.Value(1),
                                            formula.Value(2),
                                            formula.Value(3)])
        self.y_sort = formula.ConstantSort([formula.Value(2),
                                            formula.Value(3),
                                            formula.Value(4),
                                            formula.Value(5)])
        self.z_sort = formula.ConstantSort([formula.Value(1),
                                            formula.Value(2)])
        self.x: formula.Variable = formula.Variable(
                "x", self.x_sort)
        self.y: formula.Variable = formula.Variable(
                "y", self.y_sort)
        self.z: formula.Variable = formula.Variable(
                "z", self.z_sort)
        self.v_val: formula.VariableValuation = {
                self.x: formula.Value(1),
                self.y: formula.Value(5)
                }
        self.x_lq_y: formula.LessEqual = formula.LessEqual(self.x, self.y)
        self.x_lt_y: formula.Less = formula.Less(self.x, self.y)
        self.z_eq_x: formula.Equality = formula.Equality(self.z, self.x)

        self.substitution = {
                self.x: self.z,
                self.y: self.x
                }

    def test_basic_evaluation(self):
        interpretation = (self.v_val, {}, {})
        self.assertTrue(self.x_lq_y.evaluate(interpretation))
        failing_val: formula.VariableValuation = {
                self.x: formula.Value(3),
                self.y: formula.Value(2)
                }
        failing_interpretation = (failing_val, {}, {})
        self.assertFalse(self.x_lq_y.evaluate(failing_interpretation))

    def test_valuation_creation(self):
        self.assertEqual(self.x_lq_y.variables, {self.x, self.y})
        self.assertEqual(self.x_lq_y.unique_function_calls, [])
        generated_var_valuations = self.x_lq_y.all_variable_valuations({})
        self.assertEqual(len(generated_var_valuations),
                         len(self.x.values.evaluate({}))
                         * len(self.y.values.evaluate({})))
        all_valuations = self.x_lq_y.all_valuations({})
        self.assertEqual(len(all_valuations),
                         len(self.x.values.evaluate({}))
                         * len(self.y.values.evaluate({})))
        satisfying_valuations = self.x_lq_y.all_satisfying_valuations({})
        # expected:
        # x = 1, y = 2 | x = 1, y = 3 | x = 1, y = 4 | x = 1, y = 5
        # x = 2, y = 2 | x = 2, y = 3 | x = 2, y = 4 | x = 2, y = 5
        #              | x = 3, y = 3 | x = 3, y = 4 | x = 3, y = 5
        self.assertEqual(len(satisfying_valuations), 2*4 + 3)
        expected_valuations = [
                {
                    self.x: formula.Value(1),
                    self.y: formula.Value(2)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(3)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(4)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(5)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(2)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(3)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(4)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(5)
                    # }, {
                    #     self.x: formula.Value(3),
                    #     self.y: formula.Value(2)
                }, {
                    self.x: formula.Value(3),
                    self.y: formula.Value(3)
                }, {
                    self.x: formula.Value(3),
                    self.y: formula.Value(4)
                }, {
                    self.x: formula.Value(3),
                    self.y: formula.Value(5)
                }
            ]
        for v in expected_valuations:
            self.assertIn((v, {}), satisfying_valuations)

    def test_valuation_junctions(self):
        conjunction: formula.Conjunction = formula.Conjunction(self.x_lt_y,
                                                               self.z_eq_x)
        all_sat_valuations = conjunction.all_satisfying_valuations({})
        # expected:
        # x = 1, y = 2 | x = 1, y = 3 | x = 1, y = 4 | x = 1, y = 5 & z = x = 1
        #              | x = 2, y = 3 | x = 2, y = 4 | x = 2, y = 5 & z = x = 2
        # the range of z restricts x to values in [1, 2]
        expected_valuations = [
                {
                    self.x: formula.Value(1),
                    self.y: formula.Value(2),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(3),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(4),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(5),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(3),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(4),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(5),
                    self.z: formula.Value(2)
                }
            ]
        self.assertEqual(len(expected_valuations), len(all_sat_valuations))
        for v in expected_valuations:
            self.assertIn((v, {}), all_sat_valuations)

        disjunction: formula.Disjunction = formula.Disjunction(self.x_lt_y,
                                                               self.z_eq_x)
        all_sat_valuations = disjunction.all_satisfying_valuations({})
        # expected:
        # satisfying: x = z
        # x = 1, z = 1 & (y = 2 | y = 3 | y = 4 | y = 5) -> 4
        # x = 2, z = 2 & (y = 2 | y = 3 | y = 4 | y = 5) -> 4
        # satisfying: x != z & x < y
        # x = 1, z = 2 & (y = 2 | y = 3 | y = 4 | y = 5) -> 4
        # x = 2, z = 1 & (      | y = 3 | y = 4 | y = 5) -> 3
        # x = 3, (z = 1 | z = 2) & (      |       | y = 4 | y = 5) -> 2*2 = 4
        expected_valuations = [
                {
                    self.x: formula.Value(1),
                    self.y: formula.Value(2),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(3),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(4),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(5),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(2),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(3),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(4),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(5),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(2),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(3),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(4),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(1),
                    self.y: formula.Value(5),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(3),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(4),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(2),
                    self.y: formula.Value(5),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(3),
                    self.y: formula.Value(4),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(3),
                    self.y: formula.Value(5),
                    self.z: formula.Value(1)
                }, {
                    self.x: formula.Value(3),
                    self.y: formula.Value(4),
                    self.z: formula.Value(2)
                }, {
                    self.x: formula.Value(3),
                    self.y: formula.Value(5),
                    self.z: formula.Value(2)
                }
            ]
        self.assertEqual(len(expected_valuations), len(all_sat_valuations))
        for v in expected_valuations:
            self.assertIn((v, {}), all_sat_valuations)

    def test_substitution(self):
        substituted_lq = self.x_lq_y.substitute(self.substitution)
        self.assertEqual(substituted_lq, formula.LessEqual(self.z, self.x))

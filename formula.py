import typing
import dataclasses
import abc


class Sort(abc.ABC):
    @abc.abstractmethod
    def evaluate(self,
                 c_inter: "ConstantInterpretation"
                 ) -> typing.List["Value"]:
        raise NotImplementedError()


@dataclasses.dataclass(frozen=True)
class ConstantSort(Sort):
    values: typing.List["Value"]

    def evaluate(self,
                 c_inter: "ConstantInterpretation"
                 ) -> typing.List["Value"]:
        return self.values


@dataclasses.dataclass(frozen=True)
class VariableSort(Sort):
    lower_bound: "Term"
    upper_bound: "Term"

    def evaluate(self,
                 c_inter: "ConstantInterpretation"
                 ) -> typing.List["Value"]:
        lower_bound = self.lower_bound.evaluate(({}, {}, c_inter))
        upper_bound = self.upper_bound.evaluate(({}, {}, c_inter))
        return [Value(val) for val in range(lower_bound.value,
                                            upper_bound.value + 1)]


class Evaluatable(object):
    def evaluate(self, interpretation: "Evaluation") -> typing.Any:
        raise NotImplementedError()

    @property
    def variables(self) -> typing.Set["Variable"]:
        return set()

    @property
    def used_values(self) -> typing.Set["Value"]:
        return set()

    @property
    def unique_function_calls(self) -> typing.List["FunctionCall"]:
        return []

    @property
    def constants(self) -> typing.Set["Constant"]:
        return set()

    def all_variable_valuations(self,
                                c_inter: "ConstantInterpretation"
                                ) -> typing.List["VariableValuation"]:
        sorted_variables: typing.List["Variable"] = list(self.variables)
        return compute_variable_valuations(c_inter, sorted_variables)

    def all_function_valuations(self,
                                v_val: "VariableValuation",
                                c_inter: "ConstantInterpretation"
                                ) -> typing.List["FunctionValuation"]:
        free_function_calls = [f for f in self.unique_function_calls
                               if isinstance(f.function, FreeFunction)]
        return compute_function_valuations(v_val, c_inter, free_function_calls)

    def all_valuations(self,
                       c_inter: "ConstantInterpretation"
                       ) -> typing.List[typing.Tuple["VariableValuation",
                                                     "FunctionValuation"]]:
        all_var_valuations = self.all_variable_valuations(c_inter)
        all_valuations: typing.List[typing.Tuple["VariableValuation",
                                                 "FunctionValuation"]] = []
        for v_val in all_var_valuations:
            all_valuations += [(v_val, f_val)
                               for f_val in self.all_function_valuations(
                                   v_val, c_inter)]
        return all_valuations


@dataclasses.dataclass(frozen=True)
class Term(Evaluatable):
    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "Term":
        raise NotImplementedError()

    @property
    def variables(self) -> typing.Set["Variable"]:
        return self._variables  # type: ignore[attr-defined]

    @property
    def used_values(self) -> typing.Set["Value"]:
        return self._used_values  # type: ignore[attr-defined]

    @property
    def unique_function_calls(self) -> typing.List["FunctionCall"]:
        return self._unique_function_calls  # type: ignore[attr-defined]

    @property
    def constants(self) -> typing.Set["Constant"]:
        return self._constants  # type: ignore[attr-defined]


@dataclasses.dataclass(frozen=True)
class Constant(Term):
    name: str

    def __post_init__(self):
        object.__setattr__(self, "_used_values", set())
        object.__setattr__(self, "_constants", {self})
        object.__setattr__(self, "_variables", set())
        object.__setattr__(self, "_unique_function_calls", [])

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "Constant":
        return Constant(self.name)

    def evaluate(self, interpretation: "Evaluation") -> "Value":
        _, _, c_inter = interpretation
        try:
            return c_inter[self]
        except KeyError:
            raise ValueError(f"Cannot evaluate {self} under {interpretation}")

    def __str__(self) -> str:
        return self.name


@dataclasses.dataclass(frozen=True)
class Value(Term):
    value: int

    def __post_init__(self):
        object.__setattr__(self, "_constants", set())
        object.__setattr__(self, "_variables", set())
        object.__setattr__(self, "_used_values", {self})
        object.__setattr__(self, "_unique_function_calls", [])

    @property
    def used_values(self) -> typing.Set["Value"]:
        return self._used_values  # type: ignore[attr-defined]  # noqa

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "Value":
        return Value(self.value)

    def evaluate(self, interpretation: "Evaluation") -> "Value":
        return self

    def __str__(self) -> str:
        return str(self.value)


@dataclasses.dataclass(frozen=True)
class Variable(Term):
    name: str
    values: Sort = dataclasses.field(compare=False)

    def __post_init__(self):
        object.__setattr__(self, "_variables", {self})
        object.__setattr__(self, "_constants", set())
        object.__setattr__(self, "_used_values", set())
        object.__setattr__(self, "_unique_function_calls", [])

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "Variable":
        return substitution.get(self, Variable(self.name, self.values))

    def evaluate(self, interpretation: "Evaluation") -> Value:
        try:
            valuation, _, c_inter = interpretation
            value = valuation[self]
            if value not in self.values.evaluate(c_inter):
                raise ValueError(
                        f"{self} evaluates to {value} which is not part of "
                        + f"{self}'s range")
            return value
        except KeyError:
            raise ValueError(f"Cannot evaluate {self} under {interpretation}")

    def __str__(self) -> str:
        return self.name


@dataclasses.dataclass(frozen=True)
class Function():
    name: str
    arity: int = dataclasses.field(compare=False)

    def __str__(self) -> str:
        return self.name


@dataclasses.dataclass(frozen=True)
class FreeFunction(Function):
    values: Sort = dataclasses.field(compare=False)


@dataclasses.dataclass(frozen=True)
class ComputableFunction(Function):
    action: typing.Callable[
            ["ConstantInterpretation", typing.Tuple[Value, ...]],
            "Value"] = dataclasses.field(compare=False)


@dataclasses.dataclass(frozen=True)
class FunctionCall(Term):
    function: Function
    arguments: typing.Tuple[Term, ...]

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "FunctionCall":
        arguments = tuple(a.substitute(substitution) for a in self.arguments)
        return FunctionCall(self.function, arguments)

    def __post_init__(self):
        if len(self.arguments) != self.function.arity:
            raise ValueError(
                    f"function {self.name} takes {self.function.arity} "
                    + f"arguments, found {len(self.arguments)}")
        variables = set()
        collector: typing.List["FunctionCall"] = []
        used_values = set()
        constants = set()
        for arg in self.arguments:
            variables |= arg.variables
            used_values |= arg.used_values
            constants |= arg.constants
            collector += arg.unique_function_calls
        # ordering makes sure that all our arguments come before us
        collector = collector + [self]
        # make sure every function call is unique
        collector = [c for i, c in enumerate(collector)
                     if c not in collector[:i]]
        object.__setattr__(self, "_variables", variables)
        object.__setattr__(self, "_unique_function_calls", collector)
        object.__setattr__(self, "_used_values", used_values)
        object.__setattr__(self, "_constants", constants)

    @property
    def unique_function_calls(self) -> typing.List["FunctionCall"]:
        return self._unique_function_calls  # type: ignore[attr-defined]  # noqa

    @property
    def variables(self) -> typing.Set["Variable"]:
        return self._variables  # type: ignore[attr-defined]  # noqa

    @property
    def used_values(self) -> typing.Set[Value]:
        return self._used_values  # type: ignore[attr-defined]  # noqa

    @property
    def constants(self) -> typing.Set[Constant]:
        return self._constants  # type: ignore[attr-defined]  # noqa

    def evaluated_arguments(self, interpretation: "Evaluation"
                            ) -> typing.Tuple[Value, ...]:
        evaluated_arguments: typing.Tuple[Value, ...] = tuple(
                arg.evaluate(interpretation) for arg in self.arguments)
        return evaluated_arguments

    def evaluate(self, interpretation: "Evaluation") -> Value:
        v_val, f_val, c_inter = interpretation
        evaluated_arguments = self.evaluated_arguments(interpretation)
        if isinstance(self.function, ComputableFunction):
            action = self.function.action  # type: ignore[misc]  # noqa
            result = action(c_inter, evaluated_arguments)  # type: ignore
        elif (isinstance(self.function, FreeFunction)
              and (self.function, evaluated_arguments)) in f_val:
            function = typing.cast(FreeFunction, self.function)
            result = f_val[(function, evaluated_arguments)]
            if result not in function.values.evaluate(c_inter):
                raise ValueError(
                        f"Function {self.function} evaluates to "
                        + f"unexpected value {result} under {interpretation}")
        else:
            raise ValueError(f"Cannot interpret {self} under {interpretation}")
        return result

    def __str__(self) -> str:
        arguments = ", ".join([str(arg) for arg in self.arguments])
        return f"{self.function}({arguments})"


class Formula(Evaluatable):
    def evaluate(self, interpretation: "Evaluation") -> bool:
        raise NotImplementedError()

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "Formula":
        raise NotImplementedError()

    def all_satisfying_valuations(self,
                                  c_inter: "ConstantInterpretation"
                                  ) -> typing.List[typing.Tuple[
                                      "VariableValuation",
                                      "FunctionValuation"]]:
        all_valuations = self.all_valuations(c_inter)
        sat_valuations = [(v_val, f_val) for v_val, f_val in all_valuations
                          if self.evaluate((v_val, f_val, c_inter))]
        return sat_valuations


@dataclasses.dataclass(frozen=True)
class ValueFormula(Formula):
    value: bool

    def evaluate(self, interpretation: "Evaluation") -> bool:
        return self.value

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "ValueFormula":
        return ValueFormula(self.value)

    def __str__(self):
        if self.value:
            return "$true"
        else:
            return "$false"


@dataclasses.dataclass(frozen=True)
class Junction(Formula):
    left_formula: Formula
    right_formula: Formula

    def __post_init__(self):
        variables = self.left_formula.variables | self.right_formula.variables
        constants = self.left_formula.constants | self.right_formula.constants
        used_values = (self.left_formula.used_values
                       | self.right_formula.used_values)
        object.__setattr__(self, "_variables", variables)
        object.__setattr__(self, "_constants", constants)
        object.__setattr__(self, "_used_values", used_values)
        all_calls = (self.left_formula.unique_function_calls
                     + self.right_formula.unique_function_calls)
        unique_function_calls = [c for i, c in enumerate(all_calls)
                                 if c not in all_calls[:i]]
        object.__setattr__(self, "_unique_function_calls",
                           unique_function_calls)

    @property
    def used_values(self) -> typing.Set["Value"]:
        return self._used_values  # type: ignore[attr-defined]  # noqa

    @property
    def constants(self) -> typing.Set["Constant"]:
        return self._constants  # type: ignore[attr-defined]  # noqa

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "Junction":
        left = self.left_formula.substitute(substitution)
        right = self.right_formula.substitute(substitution)
        return type(self)(left, right)

    def combine(self, left_value: bool, right_value: bool) -> bool:
        raise NotImplementedError()

    def evaluate(self, interpretation: "Evaluation") -> bool:
        left_value = self.left_formula.evaluate(interpretation)
        right_value = self.right_formula.evaluate(interpretation)
        result = self.combine(left_value, right_value)
        return result

    @property
    def variables(self) -> typing.Set[Variable]:
        return self._variables  # type: ignore[attr-defined]  # noqa

    @property
    def unique_function_calls(self) -> typing.List[FunctionCall]:
        return self._unique_function_calls  # type: ignore[attr-defined]  # noqa


@dataclasses.dataclass(frozen=True)
class Disjunction(Junction):
    def combine(self, left_value: bool, right_value: bool) -> bool:
        return left_value or right_value

    def __str__(self) -> str:
        return f"({self.left_formula} | {self.right_formula})"


@dataclasses.dataclass(frozen=True)
class Conjunction(Junction):
    def combine(self, left_value: bool, right_value: bool) -> bool:
        return left_value and right_value

    def __str__(self) -> str:
        return f"({self.left_formula} & {self.right_formula})"


@dataclasses.dataclass(frozen=True)
class Implication(Junction):
    def combine(self, left_value: bool, right_value: bool) -> bool:
        return right_value or not left_value

    def __str__(self) -> str:
        return f"({self.left_formula} => {self.right_formula})"


@dataclasses.dataclass(frozen=True)
class BiImplication(Junction):
    def combine(self, left_value: bool, right_value: bool) -> bool:
        return ((right_value and left_value) or
                (not right_value and not left_value))

    def __str__(self) -> str:
        return f"({self.left_formula} <=> {self.right_formula})"


@dataclasses.dataclass(frozen=True)
class Negation(Formula):
    inner_formula: Formula

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "Negation":
        inner = self.inner_formula.substitute(substitution)
        return Negation(inner)

    def evaluate(self, interpretation: "Evaluation") -> bool:
        return not self.inner_formula.evaluate(interpretation)

    @property
    def variables(self) -> typing.Set[Variable]:
        return self.inner_formula.variables

    @property
    def used_values(self) -> typing.Set[Value]:
        return self.inner_formula.used_values

    @property
    def unique_function_calls(self) -> typing.List[FunctionCall]:
        return self.inner_formula.unique_function_calls

    def __str__(self) -> str:
        return f"~{self.inner_formula})"


@dataclasses.dataclass(frozen=True)
class BinaryComparison(Formula):
    left_term: Term
    right_term: Term

    def __post_init__(self):
        variables = self.left_term.variables | self.right_term.variables
        object.__setattr__(self, "_variables", variables)
        constants = self.left_term.constants | self.right_term.constants
        object.__setattr__(self, "_constants", constants)
        all_calls = (self.left_term.unique_function_calls
                     + self.right_term.unique_function_calls)
        unique_function_calls = [c for i, c in enumerate(all_calls)
                                 if c not in all_calls[:i]]
        object.__setattr__(self, "_unique_function_calls",
                           unique_function_calls)

    def substitute(self, substitution: typing.Mapping["Variable", "Variable"]
                   ) -> "BinaryComparison":
        left = self.left_term.substitute(substitution)
        right = self.right_term.substitute(substitution)
        return type(self)(left, right)

    def compare(self,
                left_value: "Value",
                right_value: "Value") -> bool:
        raise NotImplementedError()

    def evaluate(self, interpretation: "Evaluation") -> bool:
        left_value = self.left_term.evaluate(interpretation)
        right_value = self.right_term.evaluate(interpretation)
        return self.compare(left_value, right_value)

    @property
    def constants(self) -> typing.Set[Constant]:
        return self._constants  # type: ignore[attr-defined]  # noqa

    @property
    def variables(self) -> typing.Set[Variable]:
        return self._variables  # type: ignore[attr-defined]  # noqa

    @property
    def used_values(self) -> typing.Set[Value]:
        return self.left_term.used_values | self.right_term.used_values

    @property
    def unique_function_calls(self) -> typing.List[FunctionCall]:
        return self._unique_function_calls  # type: ignore[attr-defined]  # noqa


@dataclasses.dataclass(frozen=True)
class Equality(BinaryComparison):
    def compare(self,
                left_value: "Value",
                right_value: "Value") -> bool:
        return left_value.value == right_value.value

    def __str__(self) -> str:
        return f"{self.left_term} = {self.right_term}"


@dataclasses.dataclass(frozen=True)
class Inequality(BinaryComparison):
    def compare(self,
                left_value: "Value",
                right_value: "Value") -> bool:
        return left_value.value != right_value.value

    def __str__(self) -> str:
        return f"{self.left_term} != {self.right_term}"


@dataclasses.dataclass(frozen=True)
class LessEqual(BinaryComparison):
    def compare(self,
                left_value: Value,
                right_value: Value) -> bool:
        return left_value.value <= right_value.value

    def __str__(self) -> str:
        return f"{self.left_term} <= {self.right_term}"


@dataclasses.dataclass(frozen=True)
class Less(BinaryComparison):
    def compare(self,
                left_value: Value,
                right_value: Value) -> bool:
        return left_value.value < right_value.value

    def __str__(self) -> str:
        return f"{self.left_term} < {self.right_term}"


def compute_variable_valuations(
        c_inter: "ConstantInterpretation",
        variables: typing.Sequence["Variable"]):
    from itertools import product
    sorted_ranges: typing.List[typing.List["Value"]] = [
            [val for val in var.values.evaluate(c_inter)]
            for var in variables]
    value_choices: typing.List[typing.Tuple["Value", ...]] = list(
            product(*sorted_ranges))
    interpretations: typing.List["VariableValuation"] = [
            dict(zip(variables, value_choice))
            for value_choice in value_choices]
    return interpretations


def compute_function_valuations(
        v_val: "VariableValuation",
        c_inter: "ConstantInterpretation",
        free_function_calls: typing.List["FunctionCall"]):
    def _compute_valuations(
                current_index: int,
                gathered_valuations: typing.List["FunctionValuation"]):
        if len(free_function_calls) <= current_index:
            return gathered_valuations
        current_function_call = free_function_calls[current_index]
        new_valuations: typing.List["FunctionValuation"] = []
        for f_val in gathered_valuations:
            interpretation = (v_val, f_val, c_inter)
            computed_arguments = current_function_call.evaluated_arguments(
                    interpretation)
            index = (current_function_call.function, computed_arguments)
            if index in f_val:
                new_valuations += [dict(f_val)]
            else:
                current_function = typing.cast(
                        FreeFunction, current_function_call.function)
                values = current_function.values.evaluate(c_inter)
                new_valuations += [dict(list(f_val.items()) + [(index, v)])
                                   for v in values]
        return _compute_valuations(current_index + 1, new_valuations)
    return _compute_valuations(0, [{}])


VariableValuation = typing.Mapping[Variable, Value]
ConstantInterpretation = typing.Mapping[Constant, Value]
FunctionValuation = typing.Mapping[
        typing.Tuple[Function, typing.Tuple[Value, ...]], Value]
Evaluation = typing.Tuple[VariableValuation,
                          FunctionValuation,
                          ConstantInterpretation]
